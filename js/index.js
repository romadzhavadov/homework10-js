const tabsBtn = document.querySelectorAll('.tabs li');
const tabsContent = document.querySelectorAll('.tabs-content li');


console.log(tabsBtn)
console.log(tabsContent)


const showContent = () => {
    for (const tab of tabsBtn) {
        tab.addEventListener('click', (e) => {
            tabsBtn.forEach((element) => {
                element.classList.remove("active");
            });
            e.target.classList.add("active");
           
            let tabName = e.target.dataset.name;

            tabsContent.forEach((elem) => {
    
                // elem.classList.add("tab");
    
                if (tabName === elem.dataset.name) {
                    elem.classList.add("tabactive");
                } else {
                    elem.classList.remove("tabactive")
                }
            })
        })
    }
}

showContent()